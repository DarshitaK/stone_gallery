from django.contrib import admin

# Register your models here.
from .models import Carousel,Top_Five_Category,Product,Featured_Category,Featured_Product,Category,Project
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name','category_id')
    class Meta:
        model = Product
admin.site.register(Carousel)
admin.site.register(Top_Five_Category)
admin.site.register(Product,ProductAdmin)
admin.site.register(Featured_Product)
admin.site.register(Featured_Category)
admin.site.register(Category)
admin.site.register(Project)