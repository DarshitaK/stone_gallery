# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0027_auto_20160714_1305'),
    ]

    operations = [
        migrations.AlterField(
            model_name='carousel',
            name='img_url',
            field=models.ImageField(max_length=2000, upload_to='static/images/carousel'),
        ),
    ]
