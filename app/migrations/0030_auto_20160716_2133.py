# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0029_auto_20160714_1346'),
    ]

    operations = [
        migrations.AlterField(
            model_name='carousel',
            name='img_url',
            field=models.FileField(null=True, upload_to=b'', blank=True),
        ),
    ]
