# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0007_auto_20160705_1719'),
    ]

    operations = [
        migrations.CreateModel(
            name='Top_Six_Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=850)),
                ('name_url', models.CharField(max_length=850)),
                ('img_url', models.ImageField(max_length=2000, upload_to=b'')),
                ('description', models.CharField(max_length=2000)),
            ],
        ),
        migrations.AlterField(
            model_name='featured_category',
            name='feat_cat_id',
            field=models.ForeignKey(to='app.Top_Six_Category'),
        ),
        migrations.AlterField(
            model_name='product',
            name='category_id',
            field=models.ForeignKey(default=None, to='app.Top_Six_Category'),
        ),
    ]
