# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0030_auto_20160716_2133'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Top_Six_Category',
            new_name='Top_Five_Category',
        ),
    ]
