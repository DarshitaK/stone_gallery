
from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.template import loader,RequestContext

# Create your views here.
from .models import Product,Carousel,Category,Top_Five_Category,Featured_Category,Featured_Product,Project

def index(request):
    text = Carousel.objects.order_by('-id')
    top_cat = Top_Five_Category.objects.order_by('-id')
    pro = Product.objects.order_by('-id')
    cat = Category.objects.order_by('id')
    proj = Project.objects.all()
    # y = Category.objects.filter(category_id =xx1.id)
    context = RequestContext(request, {
          'text'    :  text,
          'top_cat' :  top_cat,
          'pro'     :  pro,
          'cat'     :  cat,
          'proj'    :  proj,
          # 'x'           :  xx1,
          # 'y'           :  y,
    })
    return render(request,'index.html',context )

# def category(request, category_id):
#     cat = Category.objects.order_by('-id')
#     template = loader.get_template("index.html")
#     context = RequestContext(request, {
#         'cat': cat,
#     })
#     return HttpResponse(template.render(context))


def category(request,category_name_url):
    xx1 = Category.objects.get(name_url=category_name_url)
    pro = Product.objects.filter(category_id=xx1.id)
    x1 = Featured_Category.objects.all()
    x2 = Top_Five_Category.objects.all()
    x3 = Product.objects.all()
    cat = Category.objects.order_by('id')

    # y = Featured_Category.objects.get(feat_cat_id=x2.id)
    context = RequestContext(request, {
        'pro': pro,
        'xx':category_name_url,
        'xx1':xx1,
        'x1' : x1,
         'x2':x2,
        'x3' :x3,
        'cat' : cat,
         # 'y':y,
    })
    return render(request,'product.html',context)

def product(request,category_name_url,product_name_url):
    xx2 = Product.objects.get(name_url=product_name_url)
    pro = Product.objects.filter(pk=xx2.id)

    cat = Category.objects.order_by('id')

    context = RequestContext(request, {
        'pro': pro,
        'cat':cat,
    })
    return render(request, 'detail.html', context)


# def category(request,categoey_name_url,product_name_url):
#     return HttpResponse("hi this is product")
#

def contact(request):
    cat = Category.objects.order_by('id')
    context = RequestContext(request,{
        'cat':cat,
    })
    return render(request,'contact.html',context)

def project(request):
    proj = Project.objects.all()
    cat = Category.objects.order_by('id')
    context = RequestContext(request, {
        'proj': proj,
        'cat' : cat,
    })
    return render(request,'projects.html',context)